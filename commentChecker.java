//------------------------------------------------------------------------------------------
import java.util.*;
import java.util.regex.*;
//------------------------------------------------------------------------------------------
class commentChecker
{
//------------------------------------------------------------------------------------------
    public static String check(String input)
    {
        input = checkOneLineComment(input);
        input = checkMultilineComment(input);
        return input;
    }
//------------------------------------------------------------------------------------------
    public static String checkOneLineComment(String input)
    {
        StringBuilder newInput = new StringBuilder();
        if (input.contains("//"))
        {
            Scanner scanner = new Scanner(input);
            String line = "";
            while (scanner.hasNextLine())
            {
                line = scanner.nextLine().trim();
                if (line.length() > 0)
                {
                    if (!(line.charAt(0) == '/' && line.charAt(1) == '/'))
                    {
                        newInput.append(line);
                        newInput.append("\n");
                    }
                }
            }
            input = newInput.toString();
        }
        return input;
    }
//------------------------------------------------------------------------------------------
    public static String checkMultilineComment(String input)
    {
        Pattern mulitlineCommentPattern = Pattern.compile("/\\*[^/]*\\*/");
        Matcher matcher = mulitlineCommentPattern.matcher(input);
        if (matcher.find())
        {
            input = matcher.replaceAll(" ");
        }
        return input;
    }
//------------------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------------------

