                    Code Guide 
----------------------------------------------------
VARIABLE DEFINITION AND DECLERATION:
    int var1;
    float var2,var3;
    int var4=20;
    char var5,var6;
----------------------------------------------------
VARIABLE USAGE;
    var1=12;
    var 3=33.5+var1;
    var2=23.4;
    var5='a';
    var1--;
----------------------------------------------------
IF SYNTAX :
    if (a<2)
    {
	a++;
    }
----------------------------------------------------
FOR SYNTAX :
    for (int i=0;i<10;i++)
    {
	float d=2.4;
	d++;
    } 
----------------------------------------------------
MAIN SYNTAX : 
main()
{
    ...
    int a,b,c;
    a=2;
    b=4*a;
    c=1;
    ...
    c++;
    ...
    if (c<2)
    {
            float k=22.7-a;
    }
    ...
    ...
    for (int var23=0;var23<10;var23++)
    {
	--a;
    }
    ...
}
----------------------------------------------------
COMMENTS : 
//single line comment
/*
 this is a multiline comment
...
...
*/
