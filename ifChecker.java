//------------------------------------------------------------------------------------------
import java.util.*;
import java.util.regex.*;
//------------------------------------------------------------------------------------------
class ifChecker
{
    static Map<String, String> tmpVarList;
    static HashSet<String> tmpErrList;
    static ArrayList<String> stack = new ArrayList<String>();
//------------------------------------------------------------------------------------------
    public static String check(String input, HashSet<String> varDefList, Map<String, String> varList, HashSet<String> errList, HashSet<String> keywords)
    {
        tmpVarList = new HashMap<String, String>(varList);
        tmpErrList = new HashSet<String>(errList);
        String ifReg1 = " *[iI][fF]";//IF
        String ifReg2 = " *\\( *([a-zA-Z_]+\\w*) *(>=?|<=?|={2}|!=) *[-+]? *\\d+ *\\)";//CONDITION - (NUM23<-40)
        String ifReg3 = "\\s*(\\{[^{}]*\\})";//IF BODY - { ANYTHING }
        String ifReg = ifReg1 + ifReg2 + ifReg3;
        Pattern ifPattern = Pattern.compile(ifReg, Pattern.MULTILINE | Pattern.DOTALL);
        Matcher matcher = ifPattern.matcher(input);
        String varName = new String();
        if (matcher.find())
        {
            MatchResult matchResult = matcher.toMatchResult();
            varName = matcher.group(1).trim();
            if (!tmpVarList.containsKey(varName))
            {
                String error = "IF Error : variable " + varName + " is used before definition .";
                tmpErrList.add(error);
            } else
            {
                try
                {
                    String result = "VVVIF Info : \n";
                    result += "your IF variable is : " + varName + "\n";
                    input = input + result;
                } catch (Exception ex)
                {
                    String error = "IF Error !\n";
                    error += "Can not recognize your IF !";
                    error += "\n-------------------------------\n";
                    tmpErrList.add(error);
                }
            }
        } else
        {
            ifPattern = Pattern.compile(ifReg1, Pattern.MULTILINE);
            matcher = ifPattern.matcher(input);
            if (matcher.find())
            {
                handleError(input, ifReg2, ifReg3, tmpErrList);
            } else
            {
                //THERE'S NO IF IN PROGRAM
                return input;
            }
        }
        errList.addAll(tmpErrList);
        return input;
    }
    //------------------------------------------------------------------------------------------
    private static void handleError(String input, String ifCond, String ifBody, HashSet<String> tmpErrList)
    {
        Pattern ifPattern = Pattern.compile(ifCond);
        Matcher matcher = ifPattern.matcher(input);
        if (matcher.find())
        {
            ifPattern = Pattern.compile(ifCond + ifBody, Pattern.MULTILINE | Pattern.DOTALL);
            matcher = ifPattern.matcher(input);
            if (matcher.find())
            {
                String error = "IF Error !\n";
                error += "IF should be followed by ()";
                error += "\n-------------------------------\n";
                tmpErrList.add(error);
                return;
            } else
            {
                String error = "IF Error !\n";
                error += "IF body should be inside { and }";
                error += "\n-------------------------------\n";
                tmpErrList.add(error);
                return;
            }
        } else
        {
            String error = "IF Error !\n";
            error += "IF Condition can only use > < >= <= and ==";
            error += "\n-------------------------------\n";
            tmpErrList.add(error);
            return;
        }
    }
}
//------------------------------------------------------------------------------------------


