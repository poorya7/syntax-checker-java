//------------------------------------------------------------------------------------------
import java.util.*;
import java.util.regex.*;
import java.io.*;
//------------------------------------------------------------------------------------------
class indenter
{
    static String spc = "     ";
    //------------------------------------------------------------------------------------------
    static String indent(String input)
    {
        String output = removeEmptyLines(input, true);
        output = indentBlocks(output);
        output = removeEmptyLines(output, false);
        return output;
    }
    //------------------------------------------------------------------------------------------
    static String removeEmptyLines(String input, boolean doTrim)
    {
        StringBuilder output = new StringBuilder();
        Scanner in = new Scanner(input);
        while (in.hasNextLine())
        {
            String line = in.nextLine();
            if (doTrim == true)
            {
                line = line.trim();
            }
            if (line.length() > 0)
            {
                line = line + "\n";
                output.append(line);
            }
        }
        return output.toString();
    }
    //------------------------------------------------------------------------------------------
    static String indentBlocks(String input)
    {
        Stack<Integer> stack = new Stack<Integer>();
        String output = new String(input);
        int i = 0;
        try
        {
            do
            {
                if (output.charAt(i) == '{')
                {
                    //START OF BLOCK
                    stack.push(i);
                } else if (output.charAt(i) == '}')
                {
                    //END OF BLOCK
                    int index1 = stack.pop();
                    //EXTRACT BLOCK
                    String block = output.substring(index1, i + 1);
                    String newBlock = addSpace(block);
                    output = output.replace(block, newBlock);
                    i += newBlock.length() - block.length();
                }
                i++;
            } while (i < output.length());
        } catch (Exception ex)
        {
        }
        return (output);
    }
//------------------------------------------------------------------------------------------
    static String addSpace(String block)
    {
        String tmp = new String();
        int index1 = block.indexOf("{");
        int index2 = block.lastIndexOf("}");
        String oldInsideBlock = block.substring(index1 + 1, index2);
        String newInsideBlock = oldInsideBlock;
        Scanner in = new Scanner(newInsideBlock);
        while (in.hasNextLine())
        {
            String line = in.nextLine();
            if (line.length() > 0)
            {
                String newLine = new String(line);
                newLine = spc + newLine;
                tmp = "\n" + tmp + newLine + "\n";
            }
        }
        block = block.replace(oldInsideBlock, tmp);
        return (block);
    }
//------------------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------------------

