import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
//---------------------------------------------------------------------
public class layoutConstraints extends GridBagConstraints
{
	public layoutConstraints(int gridx,int gridy)
	{
		this.gridx=gridx;
		this.gridy=gridy;
	}
	public layoutConstraints(int gridx,int gridy,int gridWidth,int gridHeight)
	{
		this.gridx=gridx;
		this.gridy=gridy;
		this.gridwidth=gridWidth;
		this.gridheight=gridHeight;
	}
	public layoutConstraints setAnchor(int anchor)
	{
		this.anchor=anchor;
		return this;
	}
	public layoutConstraints setFill(int fill)
	{
		this.fill=fill;
		return this;
	}
	public layoutConstraints setWeight(double weightx,double weighty)
	{
		this.weightx=weightx;
		this.weighty=weighty;
		return this;
	}
	public layoutConstraints setInsets(int distance)
	{
		this.insets=new Insets(distance,distance,distance,distance);
		return this;
	}
	public layoutConstraints setInsets(int top,int left,int bottom,int right)
	{
		this.insets=new Insets(top,left,bottom,right);
		return this;
	}
	public layoutConstraints setIpads(int ipadx,int ipady)  
	{
		this.ipadx=ipadx;
		this.ipady=ipady;
		return this;
	}
}