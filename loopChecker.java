//------------------------------------------------------------------------------------------
import java.util.*;
import java.util.regex.*;
//------------------------------------------------------------------------------------------
class loopChecker
{
    static Map<String, String> tmpVarList;
    static HashSet<String> tmpErrList;
    static ArrayList<String> stack = new ArrayList<String>();
//------------------------------------------------------------------------------------------
    public static String check(String input, HashSet<String> varDefList, Map<String, String> varList, HashSet<String> errList, HashSet<String> keywords)
    {
        boolean variableError = false;
        tmpVarList = new HashMap<String, String>(varList);
        tmpErrList = new HashSet<String>(errList);
        String forReg1 = " *[fF][oO][rR]";//FOR
        String forReg2 = " *\\( *int +([a-zA-Z_]+\\w*) *= *[-+]? *\\d+ *";//INIT - (INT NUM23=10
        String forReg3 = ";\\s*([a-zA-Z_]+\\w*) *(>=?|<=?|={2}|!=) *[-+]? *\\d+ *";//CONDITION - ;NUM23<-40
        String forReg4 = ";\\s*([a-zA-Z_]+\\w*) *(\\+{2}|\\-{2}) *\\)";//INC - DEC - ;NUM23++)
        String forReg5 = "\\s*(\\{.*\\})";//FOR BODY - { ANYTHING }
        String forReg = forReg1 + forReg2 + forReg3 + forReg4 + forReg5;
        Pattern forPattern = Pattern.compile(forReg, Pattern.MULTILINE | Pattern.DOTALL);
        Matcher matcher = forPattern.matcher(input);
        String varName1 = new String();
        if (matcher.find())
        {
            MatchResult matchResult = matcher.toMatchResult();
            varName1 = matcher.group(1).trim();
            tmpVarList.put(varName1, "int");
            try
            {
                String varName2 = matcher.group(2);
                String varName3 = matcher.group(4);
                if (!tmpVarList.containsKey(varName2))
                {
                    String error = "FOR Error : variable " + varName2 + " is used before definition .";
                    tmpErrList.add(error);
                    variableError = true;
                }
                if (!tmpVarList.containsKey(varName3))
                {
                    String error = "FOR Error : variable " + varName3 + " is used before definition .";
                    tmpErrList.add(error);
                    variableError = true;
                }
                if (variableError == false)
                {
                    String result = "VVVLoop Info : \n";
                    result += "your loop variable is : " + varName1 + "\n";
                    input = input + result;
                }

            } catch (Exception ex)
            {
                String error = "For Loop Error !\n";
                error += "Can not recognize your loop !";
                error += "\n-------------------------------\n";
                tmpErrList.add(error);
            }
        } else
        {
            forPattern = Pattern.compile(forReg1, Pattern.MULTILINE);
            matcher = forPattern.matcher(input);
            if (matcher.find())
            {
                handleError(input, forReg2, forReg3, forReg4, forReg5, tmpErrList);
            } else
            {
                //THERE'S NO FOR IN PROGRAM
                return input;
            }
        }
        varList.putAll(tmpVarList);
        errList.addAll(tmpErrList);
        return input;
    }
    //------------------------------------------------------------------------------------------
    private static void handleError(String input, String forInit, String forCond, String forInc, String forBody, HashSet<String> tmpErrList)
    {
        Pattern forPattern = Pattern.compile(forInit + forCond + forInc);
        Matcher matcher = forPattern.matcher(input);
        if (matcher.find())
        {
            String error = "For Loop Error !\n";
            error += "for body should be inside { and }";
            error += "\n-------------------------------\n";
            tmpErrList.add(error);
            return;
        } else
        {
            forPattern = Pattern.compile(forInit + forCond);
            matcher = forPattern.matcher(input);
            if (matcher.find())
            {
                String error = "For Loop Error !\n";
                error += "Third part can only use ++ and --";
                error += "\n-------------------------------\n";
                tmpErrList.add(error);
                return;
            } else
            {
                forPattern = Pattern.compile(forInit);
                matcher = forPattern.matcher(input);
                if (matcher.find())
                {
                    String error = "For Loop Error !\n";
                    error += "The condition part can only use > < >= <= ==";
                    error += "\n-------------------------------\n";
                    tmpErrList.add(error);
                    return;
                } else
                {
                    String error = "For Loop Error !\n";
                    error += "The init part should be written as INT varName=VALUE";
                    error += "\n-------------------------------\n";
                    tmpErrList.add(error);
                    return;
                }
            }
        }
    }
    //------------------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------------------
