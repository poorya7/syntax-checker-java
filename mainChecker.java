//------------------------------------------------------------------------------------------
import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
//------------------------------------------------------------------------------------------
class mainChecker
{
    static HashSet<String> tmpErrList;
//------------------------------------------------------------------------------------------
    public static String check(String input, HashSet<String> errList)
    {
        tmpErrList = new HashSet<String>(errList);
        if (input.contains("main"))
        {
            int mainIndex = input.indexOf("main");
            char beforMainChar = ' ';
            if (mainIndex > 0)
            {
                beforMainChar = input.charAt(mainIndex - 1);
            }
            if (beforMainChar == ' ' || beforMainChar == '\n' || beforMainChar == '\t')
            {
                //INDEX OF CLOSED PARANTHESES AFTER MAIN
                int closePIndex = hasParanthesesAfterMain(input);
                if (closePIndex > 0)
                {
                    input = checkBracketsForMain(input, closePIndex);
                } else
                {
                    String error = "Main should be followed by ( ).";
                    tmpErrList.add(error);
                }
            } else
            {
                String error = "before Main should be empty .";
                tmpErrList.add(error);
            }
        } else
        {
            String error = "Main function missing.";
            tmpErrList.add(error);
        }
        errList.addAll(tmpErrList);
        return input;
    }
//------------------------------------------------------------------------------------------
    public static int hasParanthesesAfterMain(String input)
    {
        int closePIndex = 0;
        input = input.trim();
        int index = input.indexOf("main") + 4;
        if (index >= input.length())
        {
            return -1;
        } else
        {
            while (index < input.length() - 1)
            {
                char c = input.charAt(index++);
                while ((c == ' ' || c == '\t') && index < input.length() - 1)
                {
                    c = input.charAt(index++);
                }
                if (c != '(')
                {
                    return -1;
                } else
                {
                    char d = input.charAt(index++);
                    while ((d == ' ' || d == '\t') && index < input.length())
                    {
                        d = input.charAt(index++);
                    }
                    if (d != ')')
                    {
                        return -1;
                    } else
                    {
                        closePIndex = index - 1;
                        return closePIndex;
                    }
                }
            }
        }
        return -1;
    }
//------------------------------------------------------------------------------------------
    public static String checkBracketsForMain(String input, int closePIndex)
    {
        input = input.trim();
        int index = closePIndex + 1;
        if (index == input.length())
        {
            String error = "main should be started with {";
            tmpErrList.add(error);
            return input;
        }
        while (index < input.length())
        {
            char c = input.charAt(index++);
            while ((c == ' ' || c == '\t' || c == '\n') && index < input.length())
            {
                c = input.charAt(index++);
            }
            if (c != '{')
            {
                String error = "main should be started with {";
                tmpErrList.add(error);
                return input;
            } else
            {
                int k = input.lastIndexOf("}");
                if (k != -1)
                {
                    //CHECKING IF ANYTHING IS WRITTEN AFTER CLOSED }
                    String tmp = input.substring(k + 1, input.length());
                    if (tmp.length() > 0)
                    {
                        String error = "can't write anything after main() { }";
                        tmpErrList.add(error);
                        return input;
                    } else
                    {
                        //NO ERROR .. FORMAT AND RETURN !
                        input = removeMain(input, index, k);
                        return input;
                    }
                } else
                {
                    String error = "Main should be terminated by }";
                    tmpErrList.add(error);
                    return input;
                }
            }
        }
        return input;
    }
    //------------------------------------------------------------------------------------------
    public static String removeMain(String input, int firstBracketIndex, int lastBracketIndex)
    {
        StringBuilder newInput = new StringBuilder();
        try
        {
            int i = input.indexOf("main");
            if (i != 0)
            {
                newInput.append(input.substring(0, i));
            }
            newInput.append(input.substring(firstBracketIndex + 1, lastBracketIndex - 1));
            if (lastBracketIndex < input.length() - 1)
            {
                newInput.append(input.substring(lastBracketIndex + 1));
            }
        } catch (Exception ex)
        {
        }
        return newInput.toString();
    }
//------------------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------------------

