//------------------------------------------------------------------------------------------
import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
//------------------------------------------------------------------------------------------
class punctuations
{
    static char[] p =
    {
        '(', '{', '[', ')', '}', ']'
    };
    static HashSet<String> tmpErrList;
    static ArrayList<String> stack = new ArrayList<String>();
//------------------------------------------------------------------------------------------
    public static void check(String input, HashSet<String> errList)
    {
        tmpErrList = new HashSet<String>(errList);
        tmpErrList.clear();
        stack.clear();
        int k1 = input.indexOf("{");
        int k2 = input.lastIndexOf("}");
        Scanner scanner = new Scanner(input);
        while (scanner.hasNext())
        {
            String oldToken = scanner.next().trim();
            String newToken = checkToken(oldToken);
            int index = input.indexOf(oldToken);
            input = input.substring(0, index) + newToken + input.substring(index + oldToken.length());
        }
        checkStack(errList);
        return;
    }
    //------------------------------------------------------------------------------------------
    public static void checkStack(HashSet<String> errList)
    {
        if (!stack.isEmpty())
        {
            String s = stack.remove(stack.size() - 1);
            String match = "";
            for (int i = 0; i < p.length; i++)
            {
                if (String.valueOf(p[i]).equals(s))
                {
                    match = String.valueOf(p[i + 3]);
                    break;
                }
            }
            String error = "missing close " + match + " .";
            tmpErrList.add(error);
        }
        errList.addAll(tmpErrList);
    }
//------------------------------------------------------------------------------------------
    public static String checkToken(String token)
    {
        for (int k = 0; k < token.length(); k++)
        {
            for (int i = 0; i < 3; i++)
            {
                if (token.charAt(k) == p[i])
                {
                    //IT'S AN OPEN PUNC :  ( { [
                    int index = token.indexOf(p[i]);
                    stack.add(String.valueOf(p[i]));
                }
            }
            for (int i = 3; i < p.length; i++)
            {
                if (token.charAt(k) == p[i])
                {
                    //IT'S A CLOSE PUNC : ) } ]
                    if (stack.contains(String.valueOf(p[i - 3])))
                    {
                        stack.remove(String.valueOf(p[i - 3]));
                    } else
                    {
                        String error = "missing open " + p[i] + " .";
                        tmpErrList.add(error);
                    }
                }
            }
        }
        return token;
    }
}
//------------------------------------------------------------------------------------------

