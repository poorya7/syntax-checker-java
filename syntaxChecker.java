//------------------------------------------------------------------------------------------
import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
//------------------------------------------------------------------------------------------
class syntaxChecker
{
    JFrame frame;
    JTextArea txtInput, txtOutput;
    final static syntaxChecker sc = new syntaxChecker();
    HashSet<String> varDefList = new HashSet<String>();
    HashSet<String> loopList = new HashSet<String>();
    HashSet<String> keywords = new HashSet<String>();
    Map<String, String> varList = new HashMap<String, String>();
    HashSet<String> errList = new HashSet<String>();
    String forResult = "";
    String ifResult = "";
//------------------------------------------------------------------------------------------
    public static void main(String args[])
    {
        sc.init();
        sc.draw();
    }
//------------------------------------------------------------------------------------------
    public void init()
    {
        String[] varDefArray = new String[]
        {
            "int", "float", "char"
        };
        String[] loopArray = new String[]
        {
            "for", "do", "while"
        };
        String[] otherKeywordsArray = new String[]
        {
            "main", "if"
        };
        for (String s : varDefArray)
        {
            varDefList.add(s);
            keywords.add(s);
        }
        for (String s : loopArray)
        {
            keywords.add(s);
            loopList.add(s);
        }
        for (String s : otherKeywordsArray)
        {
            keywords.add(s);
            loopList.add(s);
        }

    }
//------------------------------------------------------------------------------------------
    public void draw()
    {
        frame = new JFrame("Syntax Checker");
        frame.setBounds(50, 40, 900, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel inputPanel = new JPanel();
        Border tmpBorder = BorderFactory.createEtchedBorder();
        Font borderFont = new Font("serif", Font.PLAIN, 18);
        Border inputBorder = BorderFactory.createTitledBorder(tmpBorder, "enter your code here", TitledBorder.LEFT, TitledBorder.TOP, borderFont);
        inputPanel.setBorder(inputBorder);
        GridBagLayout layout = new GridBagLayout();
        inputPanel.setLayout(layout);
        JPanel outputPanel = new JPanel();
        Border outputBorder = BorderFactory.createTitledBorder(tmpBorder, "syntax check result", TitledBorder.LEFT, TitledBorder.TOP, borderFont);
        outputPanel.setBorder(outputBorder);
        outputPanel.setLayout(layout);
        JButton butCheck = new JButton(new commandListener("check syntax"));
        txtInput = new JTextArea(7, 80);
        txtInput.setMargin(new Insets(2, 5, 2, 5));
        txtInput.setFont(new Font("Serif", Font.BOLD, 14));
        txtInput.setLineWrap(true);
        txtInput.setWrapStyleWord(true);
        //txtInput.getDocument().addDocumentListener(new inputChangedListener());
        //((AbstractDocument) (txtInput.getDocument())).setDocumentFilter(new inputFilter());
        JScrollPane scrollInput = new JScrollPane(txtInput);
        scrollInput.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollInput.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        txtOutput = new JTextArea(8, 10);
        txtOutput.setMargin(new Insets(2, 5, 2, 5));
        txtOutput.setFont(new Font("Serif", Font.BOLD, 14));
        txtOutput.setLineWrap(true);
        txtOutput.setWrapStyleWord(true);
        JScrollPane scrollOutput = new JScrollPane(txtOutput);
        scrollOutput.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollOutput.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
//--MENUS------------------------------------
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic('F');
        JMenu helpMenu = new JMenu("Help");
        helpMenu.setMnemonic('H');
        JMenuItem newItem = new JMenuItem(new menuAction("New", 'N'));
        newItem.setAccelerator(KeyStroke.getKeyStroke("ctrl N"));
        JMenuItem openItem = new JMenuItem(new menuAction("Open", 'O'));
        openItem.setAccelerator(KeyStroke.getKeyStroke("ctrl O"));
        JMenuItem saveItem = new JMenuItem("Save", 'S');
        saveItem.setAccelerator(KeyStroke.getKeyStroke("ctrl S"));
        saveItem.setEnabled(false);
        JMenuItem exitItem = new JMenuItem(new menuAction("Quit", 'Q'));
        exitItem.setAccelerator(KeyStroke.getKeyStroke("ctrl Q"));
        JMenuItem generalItem = new JMenuItem(new menuAction("General Help", 'G'));
        JMenuItem aboutItem = new JMenuItem(new menuAction("About", 'A'));
        fileMenu.add(newItem);
        fileMenu.add(openItem);
        fileMenu.add(saveItem);
        fileMenu.addSeparator();
        fileMenu.add(exitItem);
        helpMenu.add(generalItem);
        helpMenu.addSeparator();
        helpMenu.add(aboutItem);
        menuBar.add(fileMenu);
        menuBar.add(helpMenu);
//-------------------------------------------
        inputPanel.add(scrollInput, new layoutConstraints(0, 0).setFill(layoutConstraints.BOTH).setAnchor(layoutConstraints.CENTER).setWeight(100, 100).setInsets(5, 10, 15, 10));
        inputPanel.add(butCheck, new layoutConstraints(0, 1).setAnchor(layoutConstraints.EAST).setInsets(0, 10, 20, 20));
        outputPanel.add(scrollOutput, new layoutConstraints(1, 0).setFill(layoutConstraints.BOTH).setAnchor(layoutConstraints.CENTER).setWeight(100, 100).setInsets(5, 10, 15, 10));
        frame.setJMenuBar(menuBar);
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(layout);
        mainPanel.add(inputPanel, new layoutConstraints(0, 0).setFill(layoutConstraints.BOTH).setWeight(100, 100).setInsets(15, 30, 30, 20));
        mainPanel.add(outputPanel, new layoutConstraints(1, 0).setFill(layoutConstraints.BOTH).setWeight(100, 100).setInsets(50, 20, 110, 40));
        frame.getContentPane().add(BorderLayout.CENTER, mainPanel);
        frame.setVisible(true);
    }
//------------------------------------------------------------------------------------------
    public void checkSyntax(String input)
    {
        input = input.trim();
        String indentedInput = new String(input);
        indentedInput = indenter.indent(indentedInput);
        txtInput.setText(indentedInput);
        if (input.length() > 0)
        {
            input = commentChecker.check(input);
            input = mainChecker.check(input, errList);
            varChecker.checkVariables(input, varDefList, errList, varList, keywords, true);
            input = ifChecker.check(input, varDefList, varList, errList, keywords);
            if (input.indexOf("VVVIF Info") >= 0)
            {
                try
                {
                    ifResult = input.substring(input.indexOf("VVVIF Info") + 3);
                    input = input.substring(0, input.indexOf("VVVIF Info"));
                } catch (Exception ex)
                {
                }
            } else
            {
                ifResult = "No IF Condition Found !\n";
            }
            input = loopChecker.check(input, varDefList, varList, errList, keywords);
            if (input.indexOf("VVVLoop Info") >= 0)
            {
                try
                {
                    forResult = input.substring(input.indexOf("VVVLoop Info") + 3);
                    input = input.substring(0, input.indexOf("VVVLoop Info"));
                } catch (Exception ex)
                {
                }
            } else
            {
                forResult = "no For Loop Found !\n";
            }
            punctuations.check(input, errList);
            showResults();
        } else
        {
            txtOutput.setForeground(new Color(70, 0, 0));
            String error = "write something please !!";
            txtOutput.setText(error);
        }
    }
//------------------------------------------------------------------------------------------'
    String makeVarResults()
    {
        Set<String> varNames = varList.keySet();
        String result = "";
        ArrayList<String> intList = new ArrayList<String>();
        ArrayList<String> floatList = new ArrayList<String>();
        ArrayList<String> charList = new ArrayList<String>();
        for (String varName : varNames)
        {
            String value = varList.get(varName);
            if (value.equalsIgnoreCase("int"))
            {
                intList.add(varName);
            } else if (value.equalsIgnoreCase("float"))
            {
                floatList.add(varName);
            } else if (value.equalsIgnoreCase("char"))
            {
                charList.add(varName);
            }
        }
        if (!intList.isEmpty())
        {
            result = "Integers ( " + intList.size() + " )" + " : ";
            for (String s : intList)
            {
                result += s + " , ";
            }
            result = result.substring(0, result.length() - 2) + "\n";
        }
        if (!floatList.isEmpty())
        {
            result += "Floats     ( " + floatList.size() + " )" + " : ";
            for (String s : floatList)
            {
                result += s + " , ";
            }
            result = result.substring(0, result.length() - 2) + "\n";
        }
        if (!charList.isEmpty())
        {
            result += "Chars     ( " + charList.size() + " )" + " : ";
            for (String s : charList)
            {
                result += s + " , ";
            }
            result = result.substring(0, result.length() - 2) + "\n";
        }
        return result;
    }
//------------------------------------------------------------------------------------------
    public void showResults()
    {
        String result = "";
        txtOutput.setText(null);
        if (errList.isEmpty())
        {
            //NO ERROR FOUND !
            txtOutput.setForeground(new Color(0, 70, 0));
            result = "success !\n------------------------\n";
            if (varList.size() > 0)
            {
                result += "you have " + varList.size() + " variables :\n";
                result += makeVarResults();
            } else
            {
                result += "you don't have any variable(s) !\n";
            }
            result += "------------------------\n" + forResult;
            result += "------------------------\n" + ifResult;
        } else
        //CODE CONTAINS ERROR
        {
            txtOutput.setForeground(new Color(70, 0, 0));
            result = "Failed...!\n";
            result += "you have " + errList.size() + " error (s) :\n";
            result += "------------------------\n";
            int counter = 1;
            for (String err : errList)
            {
                result += counter + " - " + err + "\n";
                counter++;
            }
        }
        txtOutput.setText(result);
    }
    //------------------------------------------------------------------------------------------
    class commandListener extends AbstractAction
    {
        public commandListener(String val)
        {
            putValue(Action.NAME, val);
            putValue("value", val);
        }
        public void actionPerformed(ActionEvent ev)
        {
            String s = txtInput.getText();
            varList.clear();
            errList.clear();
            forResult = "";
            ifResult = "";
            sc.checkSyntax(s);
        }
    }
//------------------------------------------------------------------------------------------
    public void showAboutWindow()
    {
        String output = "";
        Scanner in;
        try
        {
            in = new Scanner(new File("conf/abt.sc"));
            while (in.hasNextLine())
            {
                output += in.nextLine() + "\n";
            }
        } catch (FileNotFoundException ex)
        {
            output = "about file not found !";
        }
        output = output.trim();
        showInNewFrame(output, "About", true);
    }
    //------------------------------------------------------------------------------------------
    public void showHelpWindow()
    {
        String output = "";
        Scanner in;
        try
        {
            in = new Scanner(new File("conf/hlp.sc"));
            while (in.hasNextLine())
            {
                output += in.nextLine() + "\n";
            }
        } catch (FileNotFoundException ex)
        {
            output = "help file not found !";
        }
        output = output.trim();
        showInNewFrame(output, "Help", false);
    }
    //------------------------------------------------------------------------------------------
    public void showInNewFrame(String s, String title, boolean isAbout)
    {
        int fontSize = (isAbout == true) ? 18 : 14;
        int fontType = (isAbout == true) ? Font.PLAIN : Font.BOLD;
        JFrame newFrame = new JFrame(title);
        newFrame.setBounds(220, 220, 500, 400);
        newFrame.setAlwaysOnTop(true);
        JTextArea output = new JTextArea(s);
        output.setMargin(new Insets(20, 15, 20, 15));
        output.setFont(new Font("Serif", fontType, fontSize));
        output.setLineWrap(true);
        output.setWrapStyleWord(true);
        output.setEditable(false);
        JScrollPane outScroll = new JScrollPane(output);
        outScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        outScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        newFrame.add(outScroll);
        newFrame.setVisible(true);
    }
    //------------------------------------------------------------------------------------------
    public void getInputFile()
    {
        txtInput.setText("");
        JFileChooser chooser;
        String fileName = "";
        try
        {
            chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File("."));
            int result = chooser.showOpenDialog(frame);
            fileName = chooser.getSelectedFile().getPath();
        } catch (Exception ex)
        {
        }
        Scanner in;
        try
        {
            in = new Scanner(new File(fileName));
            while (in.hasNextLine())
            {
                txtInput.setText(txtInput.getText() + "\n" + in.nextLine());
            }
        } catch (FileNotFoundException ex)
        {
        }
    }
    //------------------------------------------------------------------------------------------
    class menuAction extends AbstractAction
    {
        public menuAction(String name, char hotKey)
        {
            putValue(Action.NAME, name);
            putValue(Action.MNEMONIC_KEY, new Integer(hotKey));
        }
        public void actionPerformed(ActionEvent ev)
        {
            String actionName = getValue(Action.NAME).toString().toLowerCase();
            if (actionName.equals("about"))
            {
                showAboutWindow();
            } else if (actionName.equals("quit"))
            {
                System.exit(0);
            } else if (actionName.equals("new"))
            {
                txtInput.setText("");
                txtOutput.setText("");
            } else if (actionName.equals("open"))
            {
                getInputFile();
            } else if (actionName.equals("general help"))
            {
                showHelpWindow();
            }
        }
    };
    //------------------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------------------
//////////////////////////////////////////////////////

