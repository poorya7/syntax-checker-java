//------------------------------------------------------------------------------------------
import java.util.*;
import java.util.regex.*;
//------------------------------------------------------------------------------------------
class varChecker
{
    static HashSet<String> tmpVarDefList;
    static Map<String, String> tmpVarList;
    static HashSet<String> tmpErrList;
    static HashSet<String> tmpKeywords;
//------------------------------------------------------------------------------------------
    public static void checkVariables(String input, HashSet<String> varDefList, HashSet<String> errList, Map<String, String> varList, HashSet<String> keywords, boolean excludeIfFor)
    {
        tmpVarDefList = new HashSet<String>(varDefList);
        tmpVarList = new HashMap<String, String>(varList);
        tmpErrList = new HashSet<String>();
        tmpKeywords = new HashSet<String>(keywords);
        String[] p =
        {
            "(", ")", "{", "}", "[", "]"
        };
        input = input.trim();
        if (input.contains("main"))
        {
            //EXCLUDE MAIN
            input = input.substring(0, input.indexOf("main")) + " " + input.substring(input.indexOf("main") + 4);
        }
        input = removeIfAndForHeaders(input);
        Scanner scanner = new Scanner(input);
        while (scanner.hasNextLine())
        {
            String line = scanner.nextLine().trim();
            if (line.length() > 0)
            {
                //CHECKS FOR SEMICOLON AT THE END OF THE LINE AND REMOVES IT
                line = checkEndingInSemiColon(line);
                if (line != null)
                {
                    checkLine(line);
                }
            }
        }
        errList.addAll(tmpErrList);
        varList.clear();
        varList.putAll(tmpVarList);
        return;
    }
//------------------------------------------------------------------------------------------
    public static String removeIfAndForHeaders(String input)
    {
        String ifReg = " *[iI][fF] *\\([^\\(\\)]*\\)";
        String forReg = " *[fF][oO][rR] *\\(";//FOR (
        Pattern pattern = Pattern.compile(ifReg, Pattern.MULTILINE | Pattern.DOTALL);
        Matcher matcher = pattern.matcher(input);
        String newInput = new String(input);
        if (matcher.find())
        {
            MatchResult mr = matcher.toMatchResult();
            newInput = input.substring(0, mr.start());
            newInput += input.substring(mr.end() + 1);
        }
        pattern = Pattern.compile(forReg, Pattern.MULTILINE | Pattern.DOTALL);
        matcher = pattern.matcher(newInput);
        String newInput2 = new String(newInput);
        if (matcher.find())
        {

            MatchResult mr = matcher.toMatchResult();
            int index1 = mr.start();
            int index2 = newInput.indexOf(")", index1);
            newInput2 = newInput.substring(0, index1);
            newInput2 += newInput.substring(index2 + 1);
        }
        return newInput2;
    }
//------------------------------------------------------------------------------------------
    public static String checkEndingInSemiColon(String line)
    {
        line = line.trim();
        //REMOVE { AND } FROM THE LINE
        String newLine = new String(line);
        if (newLine.contains("{"))
        {
            newLine = newLine.replaceAll("\\{", " ").trim();
        }
        if (newLine.contains("}"))
        {
            newLine = newLine.replaceAll("\\}", " ").trim();
        }
        if (newLine.length() < 1)
        {
            return null;
        } else
        {
            //IF THE LINE IS FOLLOWED BY SEMI COLON ,
            //REMOVE IT , OTHERWISE IT'S AN ERROR
            if (line.charAt(line.length() - 1) == ';')
            {
                line = line.substring(0, line.length() - 1);
                return line;
            } else
            {
                //NO SEMI COLON FOOUND
                //CHECK TO SEE THAT THE LINE DOESN'T CONTAIN IF OR FOR
                String ifReg = " *[iI][fF]";
                Pattern ifPattern = Pattern.compile(ifReg, Pattern.MULTILINE | Pattern.DOTALL);
                Matcher matcher = ifPattern.matcher(line);
                //StringBuilder varName = new StringBuilder();
                if (!matcher.find())
                {
                    ifReg = " *[fF][oO][rR]";
                    ifPattern = Pattern.compile(ifReg, Pattern.MULTILINE | Pattern.DOTALL);
                    matcher = ifPattern.matcher(line);
                    if (!matcher.find())
                    {
                        String error = "missing semi colon (;) after line " + line + " .";
                        tmpErrList.add(error);
                        return null;
                    }
                }
            }
            return null;
        }
    }
//------------------------------------------------------------------------------------------
    public static void checkLine(String line)
    {
        Scanner lineScanner = new Scanner(line);
        if (lineScanner.hasNext())
        {
            String token = lineScanner.next();
            token = token.trim();
            if (isVarKeyword(token))
            {
                if (lineScanner.hasNext())
                {
                    String varDef = line.substring(line.indexOf(token) + token.length()).trim();
                    checkVarDeclaration(varDef, token);
                } else
                {
                    String error = "variable Declaration missing after " + token + " .";
                    tmpErrList.add(error);
                }
            } else
            {
                line = line.substring(0, line.length()).trim();
                checkVarDefinition(line);
            }
        }
    }
//------------------------------------------------------------------------------------------
    public static void checkVarDefinition(String varDef)
    {
        if (varDef.contains("="))
        {
            String varName = varDef.substring(0, varDef.indexOf("="));
            varName = varName.trim();
            if (!tmpVarList.containsKey(varName))
            {
                String error = "variable " + varName + " is used before definition .";
                tmpErrList.add(error);
            } else
            {
                //CHECK IF VALUE MATCHES THE VARIABLE TYPE
                String varValue = varDef.substring(varDef.indexOf("=") + 1).trim();
                String varKeyword = tmpVarList.get(varName);
                checkTypeMatch(varDef, varName, varValue, varKeyword);
            }
        } else
        {
            // CHECKS FOR ++ OR --
            checkUnaryOperation(varDef);
        }
    }
    //------------------------------------------------------------------------------------------
    public static void checkUnaryOperation(String varDef)
    {
        String unaryOp = " *(\\+{2}|\\-{2}) *";
        Pattern uoPattern = Pattern.compile(unaryOp);
        Matcher matcher = uoPattern.matcher(varDef);
        if (matcher.find())
        {
            //CONTAINS ++ OR --
            MatchResult matchResult = matcher.toMatchResult();
            String varName = varDef.substring(0, matchResult.start()).trim();
            if (varName.length() > 0)
            {
                // VARNAME++ OR VARNAME--
                if (!tmpVarList.containsKey(varName))
                {
                    String error = "variable " + varName + " is used before definition .";
                    tmpErrList.add(error);
                }
            } else
            {
                // ++VARNAME OR --VARNAME
                varName = varDef.substring(matchResult.start() + 2).trim();
                if (varName.length() > 0)
                {
                    if (!tmpVarList.containsKey(varName))
                    {
                        String error = "variable " + varName + " is used before definition .";
                        tmpErrList.add(error);
                    }
                } else
                {
                    String error = "missing variable at line : " + varDef + " !";
                    tmpErrList.add(error);
                }
            }
        } else
        {
            String error = "unknown keyword : " + varDef + " !";
            tmpErrList.add(error);
        }
    }
//------------------------------------------------------------------------------------------
    public static boolean isLegalIdentifier(String varName)
    {
        if (tmpKeywords.contains(varName))
        {
            String error = varName + " is a keyword and can't be used for variable names.";
            tmpErrList.add(error);
            return true;
        } else
        {
            //GET THE LEFTMOST CHARACTER
            char c = varName.charAt(0);
            //IF THE LEFTMOST CHARACTER IS NOT A LETTER, THE NAME IS NOT VALID
            if (!(Character.isLetter(c)))
            {
                return false;
            } else
            {
                //IF THE LAST CHARACTER IS A COMMA THE NAME IS NOT VALID
                char ch = varName.charAt(varName.length() - 1);
                if (ch == ',')
                {
                    return false;
                } else
                {
                    for (int i = 1; i
                            < varName.length(); i++)
                    {
                        //CHECK THE REST OF THE INPUT , CHAR BY CHAR
                        ch = varName.charAt(i);
                        String tmp = ch + "";
                        //THE CHARACTER CAN BE A DIGIT , A LETTER OR AN UNDERLINE
                        if (!(Character.isDigit(ch)) && !(Character.isLetter(ch)) && !(tmp.compareTo("_") == 0))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
        }
    }
//------------------------------------------------------------------------------------------
    public static boolean isVarKeyword(String token)
    {
        if (tmpVarDefList.contains(token))
        {
            return true;
        } else
        {
            return false;
        }
    }
//------------------------------------------------------------------------------------------
    public static void checkVarDeclaration(String varDef, String varKeyword)
    {
        if (isLegalIdentifier(varDef))
        {
            addToVarList(varDef, varKeyword);
        } else if (varDef.contains(","))
        //MAY BE MULTIPLE VAR DECLERATION
        {
            checkMultiDeclaration(varDef, varKeyword);
        } else if (varDef.contains("="))
        //MAY BE VAR INITIALIZAION
        {
            checkVarInitialization(varDef, varKeyword);
        } else
        {
            String error = "variable " + varDef + " is illegal .";
            tmpErrList.add(error);
        }
    }
//------------------------------------------------------------------------------------------
    public static void checkMultiDeclaration(String varDef, String varKeyword)
    {
        //THE FIRST AND LAST CHARACTERS SHOULDN'T BE ,
        if (varDef.charAt(0) == ',')
        {
            String varName = varDef.substring(varDef.indexOf(",") + 1);
            while (varName.contains(","))
            {
                varName = varName.substring(0, varName.indexOf(","));
            }
            varName = "," + varName;
            String error = "variable " + varName + " is illegal .";
            tmpErrList.add(error);
        } else if (varDef.charAt(varDef.length() - 1) == ',')
        {
            String varName = varDef.substring(0, varDef.lastIndexOf(","));
            while (varName.contains(","))
            {
                varName = varName.substring(varName.indexOf(",") + 1);
            }
            varName = varName + ",";
            String error = "variable " + varName + " is illegal .";
            tmpErrList.add(error);
        } else
        {
            while (varDef.contains(","))
            {
                String varName = varDef.substring(0, varDef.indexOf(","));
                varName = varName.trim();
                if (varName.length() < 1)
                {
                    String error = "error .. possibly wrong number of commas !?";
                    tmpErrList.add(error);
                } else if (isLegalIdentifier(varName))
                {
                    addToVarList(varName, varKeyword);
                } else
                {
                    String error = "variable " + varName + " is illegal .";
                    tmpErrList.add(error);
                }
                varDef = varDef.substring(varDef.indexOf(",") + 1);
            } //CHECK THE LAST VARIABLE
            if (isLegalIdentifier(varDef) && varDef.length() > 0)
            {
                addToVarList(varDef, varKeyword);
            } else
            {
                String error = "variable " + varDef + " is illegal .";
                tmpErrList.add(error);
            }
        }
    }
//------------------------------------------------------------------------------------------
    public static void addToVarList(String varDef, String varKeyword)
    {
        if (!tmpVarList.containsKey(varDef))
        {
            tmpVarList.put(varDef, varKeyword);
        } else
        {
            String error = "variable " + varDef + " is already defined .";
            tmpErrList.add(error);
        }
    }
//------------------------------------------------------------------------------------------
    public static void checkVarInitialization(String varDef, String varKeyword)
    {
        String varName = varDef.substring(0, varDef.indexOf("=")).trim();
        if (isLegalIdentifier(varName))
        {
            addToVarList(varName, varKeyword);
            String varValue = varDef.substring(varDef.indexOf("=") + 1).trim();
            checkTypeMatch(varDef, varName, varValue, varKeyword);
        } else
        {
            String error = "variable " + varName + " is illegal .";
            tmpErrList.add(error);
        }
    }
//------------------------------------------------------------------------------------------
    public static void checkTypeMatchInt(String varDef, String varName, String value, String keyword)
    {
        //VALUE COULD BE : NUMBER OR VARNAME/NUMBER +/-/* VARNAME/NUMBER
        String intReg = " *(\\-|\\+)?(((([0-9]+)|([a-zA-Z_]+\\w*)) *((\\+|\\-|\\*) *(([0-9]+)|([a-zA-Z_]+\\w*))))|([0-9]+))";
        Pattern intPattern = Pattern.compile(intReg);
        Matcher matcher = intPattern.matcher(value);
        if (matcher.matches())
        {
            String var1 = matcher.group(6);
            String var2 = matcher.group(11);
            if (var1 != null)
            {
                if (!tmpVarList.containsKey(var1))
                {
                    String error = "variable " + var1 + " is used before definition .";
                    tmpErrList.add(error);
                }
            }
            if (var2 != null)
            {
                if (!tmpVarList.containsKey(var2))
                {
                    String error = "variable " + var2 + " is used before definition .";
                    tmpErrList.add(error);
                }
            }
        } else
        {
            String error = value + " is not an Integer.";
            tmpErrList.add(error);
        }
    }
    //------------------------------------------------------------------------------------------
    public static void checkTypeMatchFloat(String varDef, String varName, String value, String keyword)
    {
        //VALUE COULD BE : NUMBER OR VARNAME/NUMBER +/-/*/ VARNAME/NUMBER
        String floatReg = " *(\\-|\\+)?(((([0-9]+\\.?[0-9]*)|([a-zA-Z_]+\\w*)) *((\\/|\\+|\\-|\\*) *(([0-9]+\\.?[0-9]*)|([a-zA-Z_]+\\w*))))|([0-9]+\\.?[0-9]*))";
        Pattern floatPattern = Pattern.compile(floatReg);
        Matcher matcher = floatPattern.matcher(value);
        if (matcher.matches())
        {
            String var1 = matcher.group(6);
            String var2 = matcher.group(11);
            if (var1 != null)
            {
                if (!tmpVarList.containsKey(var1))
                {
                    String error = "variable " + var1 + " is used before definition .";
                    tmpErrList.add(error);
                }
            }
            if (var2 != null)
            {
                if (!tmpVarList.containsKey(var2))
                {
                    String error = "variable " + var2 + " is used before definition .";
                    tmpErrList.add(error);
                }
            }
        } else
        {
            String error = value + " is not a Float.";
            tmpErrList.add(error);
        }
    }
//------------------------------------------------------------------------------------------
    public static void checkTypeMatch(String varDef, String varName, String value, String keyword)
    {
        if (keyword.equalsIgnoreCase("int"))
        {
            checkTypeMatchInt(varDef, varName, value, keyword);
        } else if (keyword.equalsIgnoreCase("float"))
        {
            checkTypeMatchFloat(varDef, varName, value, keyword);
        } else if (keyword.equalsIgnoreCase("char"))
        {
            Pattern charInitPattern = Pattern.compile("\\s*\'.?\'");
            Matcher matcher = charInitPattern.matcher(value);
            if (!matcher.matches())
            {
                String error = value + " is not a char .";
                tmpErrList.add(error);
                return;
            }
        }
    }
    //------------------------------------------------------------------------------------------
}
